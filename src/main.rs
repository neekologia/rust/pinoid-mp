use std::fs::File;
use std::io::BufReader;

use iced::widget::pick_list;
use iced::Element;
use rodio::{Decoder, OutputStream, Sink};

#[derive(Default)]
struct State {
	music: String,
}

#[derive(Debug, Clone)]
enum Message {
	Selected(String),
	List(Vec<String>),
}

async fn play(music: String) {
	let file = BufReader::new(File::open(format!("music/{}", music)).unwrap());
	let source = Decoder::new(file).unwrap();

	let (_stream, stream_handle) = OutputStream::try_default().unwrap();
	let sink = Sink::try_new(&stream_handle).unwrap();
	sink.stop();
	sink.append(source);
	sink.sleep_until_end();
}

fn update(state: &mut State, message: Message) {
	match message {
		Message::Selected(music) => {
			state.music = music.clone();
			tokio::spawn(play(music));
		}
		Message::List(list) => {
			state.music = String::new();
		}
	}
}

fn view(state: &State) -> Element<'_, Message> {
	let mut entries = vec![];
	let files = std::fs::read_dir("music").unwrap();
	for file in files {
		let Ok(file) = file else { continue };
		let file_name = file.file_name().to_string_lossy().to_string();
		entries.push(file_name);
	}
	let list = pick_list(entries, Some(state.music.clone()), Message::Selected)
		.placeholder("select a music");
	list.into()
}

#[tokio::main]
async fn main() -> iced::Result {
	iced::run("mp", update, view)
}
